
let angle =0;
let greenGraphics;
let blueGraphics; 
let redGraphics; 

let ledIntensity = 10; // 0: transparent, 255: solid

let redStatus = true;
let greenStatus = true;
let blueStatus = true;

let previousFrame = -100;


function setup(){
  createCanvas(600, 600);
  background(255);

  greenGraphics = createGraphics(500, 500);
  greenGraphics.fill(0, 255, 0, 128);
  greenGraphics.noStroke();
  greenGraphics.ellipseMode(RADIUS);

  blueGraphics = createGraphics(500,500);
  blueGraphics.fill(0, 0, 255, 128);
  blueGraphics.noStroke();
  blueGraphics.ellipseMode(RADIUS);

  redGraphics = createGraphics(500, 500);
  redGraphics.fill(255, 0, 0, 128);
  redGraphics.noStroke();
  redGraphics.ellipseMode(RADIUS);


  greenGraphics.ellipse(100, 100, 5);
  redGraphics.ellipse(100,100,5);
  blueGraphics.ellipse(95, 80, 5);
}

function draw(){

  if (previousFrame < frameCount){
    tick();

    update();
  }
}

function tick(){
  if (frameCount % 25 == 0){
    redStatus = !redStatus;
  } 

  if(frameCount % 20 == 0){
    greenStatus = !greenStatus;
  }

  if (frameCount % 15 == 0) {
    blueStatus = !blueStatus;
  }

  
  background(255,1); //ezecuted 1 time per frame
  
  angle++;
}

function update(){
  if (redStatus) drawRed();
  if (greenStatus) drawGreen();
  if (blueStatus) drawBlue();
}

function drawGreen(){
  push();
  tint(255, 80);
  translate(300, 300);
  rotate(radians(angle));
  image(greenGraphics, -50, 0);
  pop();
}

function drawRed(){
  push();
  tint(255, 80);
  translate(300,300);
  rotate(radians(angle + 10));
  image(redGraphics, -50, 0);
  pop();
}

function drawBlue(){
  push();
  tint(255, 80);
  translate(300,300);
  rotate(radians(angle+10));
  image(blueGraphics, -50, 0);
  pop();
}
